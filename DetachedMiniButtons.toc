## Interface: 80200
## Title: DetachedMiniButtons
## Author: Borohir, Reinhearthe, Kjasi
## Version: <%version%>
## Notes: Detach and move minimap buttons
## OptionalDeps:
## SavedVariables: DetachedMiniButtonsData
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
Load.xml
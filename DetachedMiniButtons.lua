

DetachedMiniButtonsData = {}

local FullSizeButtons = 
{
	["MiniMapTrackingFrame"] = true,
}

local DefaultButtons = {
["MinimapZoomIn"] = "MinimapZoomIn",
["MinimapZoomOut"] = "MinimapZoomOut",
}

local _REPLACE_UPDATE_ = 
{
	["BongosMinimapFrame"] = "BongosMinimapFrame",
}

local SpecialButtons = {
 ["WIM_IconFrameButton"] = "WIM_IconFrameButton",
 ["MiniMapBattlefieldFrame"] = "MiniMapBattlefieldFrame",
 ["GameTimeFrame"] = "GameTimeFrame",
 ["GroupCalendarButton"] = "GroupCalendarButton",
 ["Gypsy_Button"] = "Gypsy_Button",
 ["WaterboyIcon"] = "WaterboyIcon",
 ["UberHealOnClick_Minimap"] = "UberHealOnClick_Minimap",
 ["GuildAdsMinimapButton"] = "GuildAdsMinimapButton",
 ["Slib_MinimapBtn"] = "Slib_MinimapBtn",
 ["Nurfed_LockButton"] = "Nurfed_LockButton",
 ["fuckoff_button"] = "fuckoff_button",
 ["SCD_Frame"] = "SCD_Frame",
 ["AM_MinimapButton"] = "AM_MinimapButton",
 ["LootoMinimapIcon"] = "LootoMinimapIcon",
 ["PoisonPouchPoisonButton"] = "PoisonPouchPoisonButton",
 ["SWPOpen"] = "SWPOpen",
 ["BongosMinimapFrame"] = "BongosMinimapFrame",
 ["ICU_Popup"] = "ICU_Popup",
 ["GuildInventory_MinimapButton"] = "GuildInventory_MinimapButton",
 ["PoliteWhisper_MiniMap"] = "PoliteWhisper_MiniMap",
 ["xcalc_minimap_button"] = "xcalc_minimap_button",
 ["CraftBotMiniIcon"] = "CraftBotMiniIcon",
 ["MiniMapTracking"] = "MiniMapTracking",
 ["MiniMapTrackingButton"] = "MiniMapTrackingButton",
 ["ShardAceHealth"] = "ShardAceHealth",
 ["ShardAceCount"] = "ShardAceCount",
 ["NXMiniMapBut"] = "NXMiniMapBut",
 ["ArmoryMinimapButton"] = "ArmoryMinimapButton",
 ["FuBarPluginAtlasLootEnhancedFrameMinimapButton"] = "FuBarPluginAtlasLootEnhancedFrameMinimapButton",
 ["FuBarPluginSunn-ViewportArtFrameMinimapButton"] = "FuBarPluginSunn-ViewportArtFrameMinimapButton",
 ["FuBarPluginPhanxChatFrameMinimapButton"] = "FuBarPluginPhanxChatFrameMinimapButton",
 ["FuBarPluginClearFont2FrameMinimapButton"] = "FuBarPluginClearFont2FrameMinimapButton",
}

local _MASKS_ = 
{
 ["AM_MinimapButton"] = "AM_MinimapFrame",
 ["CT_OptionBarFrame"] = "CT_OptionButton",
 ["FishingBuddyMinimapFrame"] = "FishingBuddyMinimapButton",
 ["RecipeRadarMinimapButtonFrame"] = "RecipeRadarMinimapButton",
 ["AccountantButtonFrame"] = "AccountantButton",
 ["OpiumButtonFrame"] = "OpiumButton",
 ["Perl_Config_ButtonFrame"] = "PerlButton",
 ["CT_RASetsFrame"] = "CT_RASets_Button",
 ["RaidRollButtonFrame"] = "RaidRollButtonFrameButton",
 ["PanzaButtonFrame" ] = "PanzaButton",
 ["YatlasButtonFrame"] = "TheYatlasButton",
 ["LazyScriptMinimapFrame"] = "LazyScriptMinimapButton",
 ["CritLineFrameMini"] = "CritLineButton",
 ["CleanMinimapButtonFrame"] = "CleanMinimapButton",
 ["MM_ButtonFrame"] = "MM_Button",
 ["Wardrobe_IconFrame"] = "Wardrobe_IconFrame",
 ["HAMinimapFrame"] = "HAMinimapButton",
 ["kkbk_miniMap"] = "kkbk_miniMapButton1",
 ["MusicPlayer_ButtonFrame"] = "MusicPlayer_Button",
 ["MiniMapMeetingStoneFrame"] = "MiniMapMeetingStoneFrame",
 ["MiniMapTracking"] = "MiniMapTrackingButton",
 ["MiniMapBattlefieldFrame"] = "MiniMapBattlefieldFrame",
 ["LfgxButtonFrame"] = "LfgxButton",
 ["BejeweledMinimapIcon"] = "BejeweledMinimapIcon",
 --["PriceMasterButtonFrame"] = "PMButton",
}

local _PARENTMOVES_ = 
{
 ["MiniMapTrackingButton"] = "MiniMapTracking",
}

local _CHILDMOVES_ = 
{
 ["MiniMapTracking"] = "MiniMapTrackingButton",
}

local _CLASS_ = "DetachedMiniButtons "
local _VERSION_ = "<%version%>"
local _nodock = false
local _lock = false
local _initialized = false
local _usegrid = 0
local _gridsize = 25
local _player = ""
local _realm = ""
local _sorted = false
local _frameList = {}
local _sortedList = {}
local _INITIALX_ = "INITIALX"
local _INITIALY_ = "INITIALY"
local _LASTX_    = "LASTX"
local _LASTY_    = "LASTY"
local _NAME_     = "NAME"
local _NODOCK_   = "NODOCK"
local _LOCK_     = "LOCK"
local _PARENT_   = "PARENT"
local _USED_     = "USED"
local _MASKED_   = {}
local _UIMAXX_   = 0
local _UIMAXY_   = 0
local _USEGRIDX_  = "USEGRIDX"
local _GRIDSIZEX_ = "GRIDSIZEX"
local _USEGRIDY_  = "USEGRIDY"
local _GRIDSIZEY_ = "GRIDSIZEY"
local _STARTHEIGHT_ = "HEIGHT"
local _STARTWIDTH_ = "WIDTH"
local _SCALE_ = "SCALE"
local _FORCESCALE_ = "FORCESCALE"
local _FORCEHIDDEN_ = "FORCEHIDDEN"
local _lastSelectedFrame = nil

local _oldFishBuddyEnter
local _rescanned = false
local _starttime = 0
local _sliderclick = false
local _realmchar = ""

local _DEBUG_    = false

local _BEFORE_HANDLERS_ = {}

function DMBS_DEBUG(s)
 if (_DEBUG_) then
	DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .. ":" .. s)
 end
end



-- --------------------------------------------------------------------
function DMBS_OnLoad(self)

	SLASH_DetachedMiniButtons1 = "/dmbs"
	SlashCmdList["DetachedMiniButtons"] = function (msg)
		DMBS_Slash(msg)
	end
	self:RegisterEvent("VARIABLES_LOADED")
	
	Minimap:HookScript("OnShow",DMBS_SHOWBTNS)
	Minimap:HookScript("OnHide",DMBS_HIDEBTNS)
end

function DMBS_showHelp()
	DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .. " " .. _VERSION_)
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " or " .. SLASH_DetachedMiniButtons1 .. " help : these messages.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " reset : set buttons back to initial positions.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " cfg : show current settings.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " scan : rescan for minimap buttons.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " config : show the config dialog.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " lock : lock buttons in place.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " unlock : unlock the buttons.")
	DEFAULT_CHAT_FRAME:AddMessage(SLASH_DetachedMiniButtons1 .. " copy : [realm]/char.")
end

function DMBS_showSet()
	local onoff = "off"
	if (DetachedMiniButtonsData[_realm][_player][_LOCK_]) then
		onoff = "on"
	end
	DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .. " locked buttons: " .. onoff)
	onoff = "off"
	if (DetachedMiniButtonsData[_realm][_player][_NODOCK_]) then
		onoff = "on"
	end
	DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .. " attempt minimap docking: " .. onoff)

end
function DMBS_Slash(msg)

	if (not msg) then
		DMBS_showHelp()
		return
	end
	if (msg == "") then
		DMBS_showHelp()
		return
	end
	if (msg == "help") then	   
		DMBS_showHelp()
		return
	end
	if (msg == "cfg") then
		DMBS_showSet()
	end
	if (msg == "scan") then
		DMBS_GetChildren(msg)
		return
	end
	if (msg == "config") then
		DMBSConfig_Show()
		return
	end

	if (msg == "test") then
		DEFAULT_CHAT_FRAME:AddMessage(UIParent:GetEffectiveScale())
		return
	end

	if (msg == "nodock") then
		DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .."-This function is currently disabled")
		DetachedMiniButtonsData[_realm][_player][_NODOCK_] = true
		return
	end
	if (msg == "dock") then
		DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .."-This function is currently disabled")
		DetachedMiniButtonsData[_realm][_player][_NODOCK_] = false
		return
	end
	if (msg == "reset") then
		DMBS_reset(_frameList)
		return
	end
	if (msg == "lock") then
		DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .."-Buttons are now locked")
		DetachedMiniButtonsData[_realm][_player][_LOCK_] = true
		return
	end
	if (msg == "unlock") then
		DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .."-Buttons are now unlocked")
		DetachedMiniButtonsData[_realm][_player][_LOCK_] = false
		return
	end
	local _,x = string.find(msg,"copy")
	if (x ~= nil) then
		DMBS_CopyCommand(msg)
	end
end

function DMBS_OnEvent(self, event)

	if (event == "VARIABLES_LOADED") then
		if (not _initialized) then


			_realm = GetRealmName()			 -- get our realm
			_player = UnitName("player") 		 -- get our player name
			_realmchar = _realm .. "/" .. _player 

			if (not DetachedMiniButtonsData) then 
				DetachedMiniButtonsData = {}
			end
			if (not DetachedMiniButtonsData[_realm]) then
				DetachedMiniButtonsData[_realm] = {}
			end
			if (not DetachedMiniButtonsData[_realm][_player]) then
				DetachedMiniButtonsData[_realm][_player] = {}
			end
			if (DetachedMiniButtonsData[_realm][_player][_NODOCK_] == nil) then
				DetachedMiniButtonsData[_realm][_player][_NODOCK_] = _nodock
			end
			if (DetachedMiniButtonsData[_realm][_player][_LOCK_] == nil) then
				DetachedMiniButtonsData[_realm][_player][_LOCK_] = _lock
			end
			if (DetachedMiniButtonsData[_realm][_player][_USEGRIDX_]  == nil) then
				DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] = _usegrid
			end
			if (DetachedMiniButtonsData[_realm][_player][_USEGRIDY_]  == nil) then
				DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] = _usegrid
			end
			if (DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_]  == nil) then
				DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_] = _gridsize
			end
			if (DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_]  == nil) then
				DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_] = _gridsize
			end

			if (DetachedMiniButtonsData[_realm][_player][_FORCESCALE_]  == nil) then
				DetachedMiniButtonsData[_realm][_player][_FORCESCALE_] = 0
			end

			_UIMAXX_ = DMBS_INTX(UIParent:GetRight() - 16)
			_UIMAXY_ = DMBS_INTY(UIParent:GetTop())

			if (FishingBuddy) then
				if (FishingBuddy.Minimap.Button_OnEnter) then
					_oldFishBuddyEnter = FishingBuddy.Minimap.Button_OnEnter
					FishingBuddy.Minimap.Button_OnEnter=DMBS_newFishBuddyEnter_OnEnter
				else
					_oldFishBuddyEnter = nil
				end
			end

			_initialized = true
			_starttime = GetTime()
			DMBS_InitList()
			--DEFAULT_CHAT_FRAME:AddMessage("HERE!!!")
			DEFAULT_CHAT_FRAME:AddMessage(_CLASS_ .." " .. _VERSION_ .. " loaded.")
		end
	end
end

function DMBS_newFishBuddyEnter_OnEnter()
   if ( GameTooltip.fbmmbfinished ) then
      return
   end
   if ( FishingBuddy.GetSetting("UseButtonHole") == 0 ) then
      GameTooltip.fbmmbfinished = 1
      GameTooltip:SetOwner(FishingBuddyMinimapButton, "ANCHOR_LEFT")
      GameTooltip:AddLine(FishingBuddy.NAME)
      local text = FishingBuddy.TooltipBody("MinimapClickToSwitch")
      GameTooltip:AddLine(text,.8,.8,.8,1)
      GameTooltip:Show()
   end
end


function DMBS_GETVALUE(s)
	return(getglobal(s))
end

function DMBS_cleanUp(self)
	local frame =DMBS_GETVALUE("RecipeRadarMinimapButtonHighlightFrame")

	if (frame) then
		frame:Hide()
	end

	frame = DMBS_GETVALUE("TrinityBarsMinimapButton")
	if (frame) then
		frame:Show()
	end

	frame =DMBS_GETVALUE("RogueTrackerPoisonButton")

	if (frame) then
		local c =DMBS_GETVALUE("RogueTrackerPoisonText")
		if (c) then
			c:SetParent(frame)
		end
	end
	frame =DMBS_GETVALUE("RogueTrackerBlindButton")
	if (frame) then
		local c =DMBS_GETVALUE("RogueTrackerBlindText")
		if (c) then
			c:SetParent(frame)
		end
	end
	frame =DMBS_GETVALUE("RogueTrackerVanishButton")
	if (frame) then
		local c =DMBS_GETVALUE("RogueTrackerVanishText")
		if (c) then
			c:SetParent(frame)
		end
	end
	frame =DMBS_GETVALUE("RogueTrackerFlashButton")
	if (frame) then
		local c =DMBS_GETVALUE("RogueTrackerFlashText")
		if (c) then
			c:SetParent(frame)
		end
	end
	frame =DMBS_GETVALUE("RogueTrackerTeaButton")
	if (frame) then
		local c =DMBS_GETVALUE("RogueTrackerTeaText")
		if (c) then
			c:SetParent(frame)
		end
	end

	if (fuckoff_button_updateposition) then
		fuckoff_button_updateposition = function() end
	end

	if (fuckoff_onupdate) then
		fuckoff_onupdate = function() end
	end
	frame =DMBS_GETVALUE("fuckoff_button")
	if (frame) then
		frame:SetScale(1)
	end
	frame =DMBS_GETVALUE("SCD_Frame")
	if (frame) then
		frame:SetScale(1)
		frame:SetFrameLevel(1)
		frame:SetFrameStrata("LOW")
	end

	frame = DMBS_GETVALUE("PoisonPouchPoisonButton")
	if (frame) then
		frame:SetScale(1)
	end

	frame =DMBS_GETVALUE("FTM_NoTrackingFrame")
	if (frame) then
		MiniMapTrackingFrame:SetFrameLevel(1)
	end


end

function DMBS_DoInit()
	local list = {}

	list = DMBS_AddDefaults(list)
	list = DMBS_findFrames(list)
	--DMBS_resolveInitialValues(list)
	DMBS_setMovable(list)
	DMBS_setInitialRange(list)
	DMBS_rePosition(list)
	DMBS_cleanUp()
end
-- -------------------------------------------------------------------
-- -------------------------------------------------------------------
-- HELPER FUNCTIONS
-- -------------------------------------------------------------------
function DMBS_INTX(x)
	if (x == nil) then
		return(0)
	end
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] == 1) then
		local ix = math.floor( (x / DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_]) + 0.5)
		return( ix * DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_])
	end

	return(math.floor(x + 0.5))
end

function DMBS_INTY(x)
	if (x == nil) then
		return(0)
	end
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] == 1) then
		local ix = math.floor( (x / DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_]) + 0.5)
		return( ix * DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_])
	end

	return(math.floor(x + 0.5))
end

function DMBS_INRANGEX(x)
	--DEBUG(_UIMAXX_)
	if (x == nil) then
	  return(true)
	end
	if (x < 0) or (x > _UIMAXX_) then
		return(false)
	end
	return(true)
end

function DMBS_INRANGEY(y)
	--DEBUG(_UIMAXY_)
	if (y == nil) then
	  return(true)
	end
	if (y < 0) or (y > _UIMAXY_) then
		return(false)
	end
	return(true)
end

function DMBS_INRANGEXY(x,y)
	if (x == nil) or (y == nil) then
	  return(true)
	end
	if (x < 0) or (x > _UIMAXX_) or (y < 0) or (y > _UIMAXY_) then
		return(false)
	end

	return(true)
end

function DMBS_INRANGEFRAME(frame)
	if (frame) then
		if (frame.GetLeft and frame.GetTop) then
			local left = DMBS_INTX(frame:GetLeft())
			local top = DMBS_INTY(frame:GetTop())
			if (left ~= nil) and (top ~= nil) then
				if (left < -8) or (left > _UIMAXX_ - 8) or (top < -8) or (top > _UIMAXY_ - 8) then
					--DEBUG("NOT IN RANGE:" .. frame:GetName())
					return(false)
				end
			end
		end
	end
	return(true)
end

function DMBS_RANGEX(x)
 if (x == nil) then
  return(nil)
 end
 if (x < -8) then
   return(x)
 end
 if (x > _UIMAXX_) then
  return(_UIMAXX_ - 2)
 end 
 return(x)
end

function DMBS_RANGEY(x)
 if (x == nil) then
  return(nil)
 end
 if (x < -8) then
   return(x)
 end
 if (x > _UIMAXY_ ) then
  return(_UIMAXY_)
 end 
 return(x)
end

function DMBS_HIDEBTNS()
	local list = _frameList
	for framename in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][framename]) then
			local frame = DMBS_GETVALUE(framename)
			if (frame) then
				local vis = frame:IsVisible()
				DetachedMiniButtonsData[_realm][_player][framename]["visible"] = vis
				frame:Hide()
			end
		end
	end
end

function DMBS_SHOWBTNS()
	local list = _frameList
	for framename in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][framename]) then
			local frame = DMBS_GETVALUE(framename)
			if (frame) then
				local b = DetachedMiniButtonsData[_realm][_player][framename]["visible"]
				if b == true then
					frame:Show()
				end
			end
		end
	end
end

-- --------------------------------------------------------------------
-- 
-- --------------------------------------------------------------------

local EXCLUDES = {
 ["PetPaperDoll"] = 1,
 ["CompanionButton"] = 1,
 ["Chinchilla_Wheel"] = 1,
 ["PetAction"] = 1,
 ["ACWFrame"] = 1,
 ["SW_IconFrame"] = 1,
 ["TownGuardMiniButton"] = 1,
 ["SMMiniMapButtonFrame"] = 1,
 ["SMMiniMapButton"] = 1,
 ["PMButton"] = 1,
 ["PriceMasterButtonFrame"] = 1,
 ["RicoMinimap_CoordinatesFrame"] = 1,
 ["GatherNote"] = 1,
 ["GatherMiniNote"] = 1,
 ["PoisonPouch"] = 1,
 ["IFrameManagerButton"] = 1,
 ["MM_CoreFrame"] = 1,
 ["MiniMapBattlefieldDropDown"] = 1,
 ["simpleMinimapFrame"] = 1,
 ["Sprocket"] = 1,
 ["ActionBar"] = 0, 
 ["BonusActionButton"] = 0, 
 ["MainMenu"] = 0, 
 ["ShapeshiftButton"] = 0, 
 ["MultiBar"] = 0, 
 ["KeyRingButton"] = 0, 
 ["PlayerFrame"] = 0, 
 ["TargetFrame"] = 0, 
 ["PartyMemberFrame"] = 0, 
 ["ChatFrame"] = 0, 
 ["ExhaustionTick"] = 0, 
 ["TargetofTargetFrame"] = 0, 
 ["WorldFrame"] = 0, 
 ["ActionButton"] = 0, 
 ["MicroButton"] = 1, 
 ["CharacterBag"] = 0, 
 ["PetFrame"] = 0,  
 ["UIParent"] = 0, 
 ["WorldFrame"] = 0,
 ["BuffButton"]  = 0, 
 ["BuffFrame"] = 0,
}

function DMBS_isExclude(s)
  for name,x in pairs(EXCLUDES) do
    if (x == 1) then
	    if (string.find(s,name) ~= nil) then
		    return(true)
	    end    
	  end
    if (x == 0) then
      if (name == s) then
        return(true)
      end
    end
  end
	if (string.find(s,"RogueTracker") ~= nil) and (string.find(s,"DraggingFrame") ~= nil) then
		return(true)
	end
	return(false)
end


local INCLUDES = {
  ["LibDBIcon"] = 1,
	["SbQuestIcon"] = 1,
	["WIM"] = 1,
	["UI"] = 1,
	["Frame"] = 1,
	["Button"] = 1,
	["CT_"] = 1,
	["CTA_MinimapIcon"] = 1,
	["MetaMap"] = 1,
	["HunterVSNefarianIcon"] = 1,
	["mgames_minimap"] = 1,
	["kkbk_"] = 1,
	["MeetingStoneEye"] = 1,
}
function DMBS_isInclude(s)
	for name,x in pairs(INCLUDES) do
    if (x == 1) then
	    if (string.find(s,name) ~= nil) then
		    return(true)
	    end    
	  end
    if (x == 0) then
      if (name == s) then
        return(true)
      end
    end
  end

	return(false)
end

function DMBS_Mask(n)
	if (_MASKS_[n]) then
		_MASKED_[_MASKS_[n]] = n
		return(_MASKS_[n])
	end
	return(n)
end

function DMBS_ResolveMask(n,frame)
	if (_MASKS_[n]) then
		_MASKED_[_MASKS_[n]] = n
		return(_MASKS_[n])
	end
	local children = { frame:GetChildren()}
	--DEBUG("CHILDREN:" .. n .. table.getn(children))
	if (table.getn(children) == 1) then
		--DEBUG("ONE CHILD: " .. n)
		local child = children[1]
		_MASKS_[n] = child:GetName()
		return(child:GetName())
	end

	return(n)
end

function DMBS_AddDefaults(list)
	for n in pairs(DefaultButtons) do
		if (DMBS_AddFrameName(n,n)) then

			local frame =DMBS_GETVALUE(n)
			if (frame) then
				frame:SetScale(1)
			end
			list[n] = n
		end
	end
	return(list)
end

function DMBS_AddFrameName(n,p)
	local frame =DMBS_GETVALUE(n)
	--DEFAULT_CHAT_FRAME:AddMessage(frame)
	if (frame) then
		if ((not frame.GetLeft) and (not frame.GetTop)) then
			return(false)
		end
		if (frame:GetLeft() == nil) then
			return(false)
		end
		if (frame:GetTop() == nil) then
			return(false)
		end

		if (not _frameList[n]) then
			_frameList[n] = n
			table.insert(_sortedList,n)
		end
		if (not DetachedMiniButtonsData[_realm][_player][n]) then
			DetachedMiniButtonsData[_realm][_player][n] = {}
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_NAME_]) then
			DetachedMiniButtonsData[_realm][_player][n][_NAME_] = n
		end

		local sx = DMBS_INTX(frame:GetLeft() * UIParent:GetEffectiveScale())
		local sy = DMBS_INTY(frame:GetTop() * UIParent:GetEffectiveScale())
		if (not DetachedMiniButtonsData[_realm][_player][n][_INITIALX_]) then
			DetachedMiniButtonsData[_realm][_player][n][_INITIALX_] = sx
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_INITIALY_]) then
			DetachedMiniButtonsData[_realm][_player][n][_INITIALY_] = sy									  
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_LASTX_]) then
			frame.currentX = sx
			DetachedMiniButtonsData[_realm][_player][n][_LASTX_] = sx
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_LASTY_]) then
			frame.currentY = sy
			DetachedMiniButtonsData[_realm][_player][n][_LASTY_] = sy								  
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_PARENT_]) then
			DetachedMiniButtonsData[_realm][_player][n][_PARENT_] = p									  
		end
		if (not DetachedMiniButtonsData[_realm][_player][n][_USED_]) then
			DetachedMiniButtonsData[_realm][_player][n][_USED_] = false									  
		end
		frame.isdbm = true
		frame.maskname = DMBS_Mask(n)
		return(true)
	end
	return(false)
end

function DMBS_findFrames(list)
	if (Minimap.GetChildren) then
		local children = { Minimap:GetChildren()}
		if (children) then
			for _, child in pairs(children) do
				if (child.GetName and child:GetName()) then
					local n = child:GetName()
                    local p = n

					if DMBS_isInclude(n) and not DMBS_isExclude(n) then
						 --DEFAULT_CHAT_FRAME:AddMessage(n)
						 n = DMBS_ResolveMask(n,child)						 
						 if (DMBS_AddFrameName(n,p)) then
							list[n] = n
						end
					end
				end
			end
		end
	end
	if (MinimapBackdrop.GetChildren) then
		local children = { MinimapBackdrop:GetChildren()}
		if (children) then
			for _, child in pairs(children) do
				if (child.GetName and child:GetName()) then
					local n = child:GetName()
					local p = n
					if DMBS_isInclude(n) and not DMBS_isExclude(n) then
						 --DEFAULT_CHAT_FRAME:AddMessage(n)
						 n = DMBS_ResolveMask(n,child)
						 if (DMBS_AddFrameName(n,p)) then
							list[n] = n
						end
					end
				end
			end
		end
	end

	for n in pairs(SpecialButtons) do
		local s = n
		local frame =DMBS_GETVALUE(s)
		if (frame) then

			if (DMBS_AddFrameName(s,s)) then
				list[s] = s
			end
		end
	end
	return(list)
end

function DMBS_resolveInitialValues(list)
	for framename in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_] and
		    DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_]) then

			if (not DMBS_INRANGEX(DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_])) then
				DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_] =
				DMBS_INTX(DMBS_RANGEX(DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_])) - 10
			end
			if (not DMBS_INRANGEY(DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_])) then			  
				DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_] = 
				DMBS_INTY(DMBS_RANGEY(DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_])) - 10
			end
		end
		if (DetachedMiniButtonsData[_realm][_player][framename][_SCALE_] == nil) then
			DetachedMiniButtonsData[_realm][_player][framename][_SCALE_] = 1
		end
	end
end

function DMBS_setInitialRange(list)
	local frame
	for n in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][n]) then
		if (not DetachedMiniButtonsData[_realm][_player][n][_LASTX_] and 
			not DetachedMiniButtonsData[_realm][_player][n][_LASTY_]) then
				--
			frame =DMBS_GETVALUE(n)
			if (frame) then 
				if (not DMBS_INRANGEFRAME(frame)) then
					frame.currentX = DMBS_INTX(DMBS_RANGEX(frame:GetLeft())) - 16
					frame.currentY = DMBS_INTY(DMBS_RANGEY(frame:GetTop()))
					frame:ClearAllPoints()
					frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX, frame.currentY)
					DetachedMiniButtonsData[_realm][_player][n][_INITIALX_] = DMBS_INTX(frame:GetLeft())
					DetachedMiniButtonsData[_realm][_player][n][_INITIALY_] = DMBS_INTY(frame:GetTop())									  
				end
			end
		end
		end
	end
end


function DMBS_SetBeforeUpdate(frame,framename)
	if (_REPLACE_UPDATE_[framename] ~= nil) then
		return
	end
	if (_BEFORE_HANDLERS_[framename] ~= nil) then
		return
	end
	local f = frame:GetScript("OnUpdate")
	--DEFAULT_CHAT_FRAME:AddMessage("HANDLER: " .. framename)
	if (f ~= nil) then
		_BEFORE_HANDLERS_[framename] = f
	end
	frame:SetScript("OnUpdate",DMBS_OnUpdate)
end

function DMBS_setMovable(list)

	for framename in pairs(list) do
		local frame =DMBS_GETVALUE(framename)
		if (frame) then

			frame:RegisterForDrag("LeftButton")
			if (framename ~= "ShardAceCount") then
				frame:SetParent(UIParent)
			else
				frame:SetScale(1)
				local f = getglobal("SAHeader")
				if (f) then
					f:SetParent(UIParent)
					f:SetScale(1)
				end
			end
			frame:SetMovable(1)
			frame.currentX = DMBS_RANGEX(DMBS_INTX(frame:GetLeft()))
			frame.currentY = DMBS_RANGEY(DMBS_INTY(frame:GetTop()))
			--DEFAULT_CHAT_FRAME:AddMessage("FRAMENAME: " .. framename)
			if (framename == "GameTimeFrame") then
				frame:SetFrameLevel(0)
			end
			if (framename == "GroupCalendarButton") then
				frame:SetFrameLevel(1)
			end

			if (framename == "MiniMapTracking") then
				frame:SetFrameLevel(4)
			end

			if (framename == "MiniMapTrackingButton") then
				frame:SetFrameLevel(5)
			end

			local s = nil

			--Sea.util.hook( framename, "DMBS_Master_OnDragStart", "replace", "OnDragStart" )
			--Sea.util.hook( framename, "DMBS_Master_OnDragStop", "replace", "OnDragStop" )
			frame:SetScript("OnDragStart",DMBS_Master_OnDragStart)
			frame:SetScript("OnDragStop",DMBS_Master_OnDragStop)

			if (framename == "Nurfed_LockButton") then
			--	Sea.util.hook( framename, "DMBS_OnUpdate", "replace", "OnUpdate" )
				frame:SetScript("OnUpdate",DMBS_OnUpdate)
			else
			--	Sea.util.hook( framename, "DMBS_OnUpdate", "before", "OnUpdate" )
				DMBS_SetBeforeUpdate(frame,framename)
			end



		end
	end
end

function DMBS_SetScale(scale,frame,framename)

	if (DetachedMiniButtonsData[_realm][_player][framename][_STARTHEIGHT_] == nil) then
		return
	end
	if (DetachedMiniButtonsData[_realm][_player][framename][_STARTWIDTH_] == nil) then
		return
	end
	if (scale == nil) then
		return
	end
	local h = DetachedMiniButtonsData[_realm][_player][framename][_STARTHEIGHT_] * scale
	local w = DetachedMiniButtonsData[_realm][_player][framename][_STARTWIDTH_] *scale
	frame:SetHeight(h)
	frame:SetWidth(w)

end

function DMBS_ParentMove(framename,x,y)
  if (_CHILDMOVES_[framename]) then
	local f = getglobal(_CHILDMOVES_[framename])
	if (f) then
		local frame = getglobal(framename)
		if (frame) then
			frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",f:GetLeft(),f:GetTop())
		end
	end
	return
  end

  if (_PARENTMOVES_[framename] == nil) then
	return
  end
  local f = getglobal(_PARENTMOVES_[framename])
  if (f) then
	f:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",x,y)
  end

end

function DMBS_rePosition(list)
	for framename in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][framename]) then
			if (DetachedMiniButtonsData[_realm][_player][framename][_LASTX_] and 
				DetachedMiniButtonsData[_realm][_player][framename][_LASTY_]) then

				local frame =DMBS_GETVALUE(framename)
				if (frame) then
					if (DetachedMiniButtonsData[_realm][_player][_FORCESCALE_] == 1) then
						frame:SetScale(1)
					end
					if (DetachedMiniButtonsData[_realm][_player][framename][_FORCEHIDDEN_] == nil) then
						DetachedMiniButtonsData[_realm][_player][framename][_FORCEHIDDEN_] = 0
					end
					if (DetachedMiniButtonsData[_realm][_player][framename][_LOCK_] == nil) then
						DetachedMiniButtonsData[_realm][_player][framename][_LOCK_] = 0
					end
					frame:ClearAllPoints()
					frame.currentX = DMBS_INTX(DetachedMiniButtonsData[_realm][_player][framename][_LASTX_])
					frame.currentY = DMBS_INTY(DetachedMiniButtonsData[_realm][_player][framename][_LASTY_])
					frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX,frame.currentY)
					DMBS_ParentMove(framename,frame.currentX,frame.currentY)
				    DetachedMiniButtonsData[_realm][_player][framename][_LASTX_] = DMBS_INTX(frame:GetLeft())
					DetachedMiniButtonsData[_realm][_player][framename][_LASTY_] = DMBS_INTY(frame:GetTop())
					DetachedMiniButtonsData[_realm][_player][framename][_STARTHEIGHT_] = frame:GetHeight()
					DetachedMiniButtonsData[_realm][_player][framename][_STARTWIDTH_] = frame:GetWidth()
					if (DetachedMiniButtonsData[_realm][_player][framename][_SCALE_] ~= 1) then
						DMBS_SetScale(DetachedMiniButtonsData[_realm][_player][framename][_SCALE_],frame,framename)
					end
					if (DetachedMiniButtonsData[_realm][_player][framename][_FORCEHIDDEN_] == 1) then
						frame:Hide()
					end
				end
			end
		end
	end
end

function DMBS_reset(list)
	for framename in pairs(list) do
		if (DetachedMiniButtonsData[_realm][_player][framename]) then
			if (DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_] and 
				DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_]) then

				local frame =DMBS_GETVALUE(framename)
				if (frame) then
					frame:ClearAllPoints()
					frame.currentX = DMBS_RANGEX(DMBS_INTX(DetachedMiniButtonsData[_realm][_player][framename][_INITIALX_]))
					frame.currentY = DMBS_RANGEY(DMBS_INTY(DetachedMiniButtonsData[_realm][_player][framename][_INITIALY_]))
					frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX,frame.currentY)
					DetachedMiniButtonsData[_realm][_player][framename][_LASTX_] = DMBS_INTX(frame:GetLeft())
					DetachedMiniButtonsData[_realm][_player][framename][_LASTY_] = DMBS_INTY(frame:GetTop())
				end
			end
		end
	end
end

function DMBS_GetChildren(msg)
	local count = 0
	local newlist = {}
	if (msg == nil) then
	 msg = "text"
	end

	if (msg ~= "silent") then
		DEFAULT_CHAT_FRAME:AddMessage("Searching for new Minimap Buttons")
	end

	if (Minimap.GetChildren) then
		local children = { Minimap:GetChildren()}
		if (children) then
			for _, child in pairs(children) do
				if (child.GetName and child:GetName()) then
					local n = child:GetName()
					local p = n
					if DMBS_isInclude(n) and not DMBS_isExclude(n) then
						n = DMBS_ResolveMask(n,child)
						if (not _frameList[n]) then
							count = count + 1
							newlist[n] = n
							DMBS_AddFrameName(n,p)
						end
					end
				end
			end
		end
	end

	if (MinimapBackdrop.GetChildren) then
		local children = { MinimapBackdrop:GetChildren()}
		if (children) then
			for _, child in pairs(children) do
				if (child.GetName and child:GetName()) then
					local n = child:GetName()
					local p = n
					if DMBS_isInclude(n) and not DMBS_isExclude(n) then
						n = DMBS_ResolveMask(n,child)
						if (not _frameList[n]) then
							count = count + 1
							newlist[n] = n
							DMBS_AddFrameName(n,p)
						end
					end
				end
			end
		end
	end

	if (count > 0) then
		if (msg ~= "silent") then
			DEFAULT_CHAT_FRAME:AddMessage("Found " .. count .. " new buttons")
		end
		DMBS_setInitialRange(newlist)
		DMBS_setMovable(newlist)
		DMBS_rePosition(newlist)
	else
		if (msg ~= "silent") then
			DEFAULT_CHAT_FRAME:AddMessage("No new buttons found")
		end
	end
end

-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
function DMBS_Master_OnDragStart(self)
    if (DetachedMiniButtonsData[_realm][_player][_LOCK_]) then
		return
	end
	local n = self:GetName()
	if (DetachedMiniButtonsData[_realm][_player][n][_LOCK_] == 1) then
		return
	end
	self.isDragging = true
	self.dragme = true
	self:StartMoving()
end

function DMBS_Master_OnDragStop(self)
	self.isDragging = false
	self.dragme = false
	self:StopMovingOrSizing()
	local n = self:GetName()
	DetachedMiniButtonsData[_realm][_player][n][_LASTX_] = DMBS_INTX(self:GetLeft())
	DetachedMiniButtonsData[_realm][_player][n][_LASTY_] = DMBS_INTY(self:GetTop())
end

function DMBS_OnUpdate(self,elapsed)

  if (InCombatLockdown()) then
    return
   end

	if (not _initialized) then
		return
	end

	if (not _rescanned) then
		if ((GetTime() - _starttime) > 20) then
			_rescanned = true
			DMBS_DoInit()
			return
		end
	end

	if (not self.isdbm) then
		return
	end

	local n = self.maskname
    if (not n) then 
		return
	end

	if (not _frameList[n]) then
		return
	end


	if (self.isDragging) then
		--DEFAULT_CHAT_FRAME:AddMessage(n)
		local xpos,ypos = GetCursorPosition()
		local xmin,ymin = UIParent:GetLeft() , UIParent:GetBottom()

		local IconPosX = xpos / UIParent:GetEffectiveScale() - xmin
		local IconPosY = ypos / UIParent:GetEffectiveScale() - ymin

		local sc = self:GetScale()
		if (sc <= 1) and (n ~= "ShardAceCount") then
			IconPosY = (IconPosY + (self:GetWidth() / 2)) / sc
			IconPosX = (IconPosX - (self:GetWidth() / 2)) / sc
		else
			IconPosY = (IconPosY + (self:GetWidth() / 2))
			IconPosX = (IconPosX - (self:GetWidth() / 2))
		end

		self:ClearAllPoints()
		self:SetPoint("TOPLEFT",UIParent,"BOTTOMLEFT",DMBS_INTX(IconPosX), DMBS_INTY(IconPosY))

		self.currentX = DMBS_INTX(self:GetLeft())
		self.currentY = DMBS_INTY(self:GetTop())

		if (n == "GameTimeFrame") then
		  self:SetFrameLevel(0)
		end
		if (string.find(n,"RogueTracker")) then
			self:SetFrameLevel(1)
		end
		DMBS_ParentMove(n,self.currentX,self.currentY)

	else
		if (DetachedMiniButtonsData[_realm][_player][n][_FORCEHIDDEN_] == 1) then
			self:Hide()
		end
		--else
			--if (self:IsVisible()) then
			--	local l = self:GetLeft()
			--	local t = self:GetTop()
			--	local ef = UIParent:GetEffectiveScale()
			--	if (l and t) then
			--		local sx = DMBS_INTX(l * ef)
			--		local sy = DMBS_INTY(l * ef)
			--		if ((self.currentX ~= sx) and (self.currentY ~= sy)) then
			--			self:ClearAllPoints()
			--			self:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",self.currentX, self.currentY)
			--			DMBS_ParentMove(n,self.currentX,self.currentY)
			--		end
			--	end
			--end
		--end
	end
	if (_BEFORE_HANDLERS_[n]) then
		_BEFORE_HANDLERS_[n](self,elapsed)
	end
end
-- ========================================================================


function DMBSConfig_Show(self)
	DMBS_TAB1Set()
	DMBSConfigFrame:Show()
end

function DMBS_GridSliderChangedX(self)
	if (not _initialized) then
		return
	end
	local x = DMBS_GRIDSLIDERX:GetValue()
	DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_] = x
	DMBSTAB1_GRIDSIZETEXTX:SetText("Grid Size X: " .. DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_])
end

function DMBS_GridSliderChangedY(self)
	if (not _initialized) then
		return
	end
	local x = DMBS_GRIDSLIDERY:GetValue()
	DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_] = x
	DMBSTAB1_GRIDSIZETEXTY:SetText("Grid Size Y: " .. DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_])
end

function DMBSConfig_OnLoad(self)
	self:RegisterForDrag("LeftButton")
	PanelTemplates_SetNumTabs(DMBSConfigFrame, 2)
	DMBSConfigFrame.selectedTab=1
end

function DMBS_ToggleGridX()
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] == 1) then
		DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] = 0
	else
		DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] = 1
	end
end

function DMBS_ToggleGridY()
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] == 1) then
		DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] = 0
	else
		DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] = 1
	end
end

function DMBS_ToggleForceScale()
	if (DetachedMiniButtonsData[_realm][_player][_FORCESCALE_] == 1) then
		DetachedMiniButtonsData[_realm][_player][_FORCESCALE_] = 0
	else
		DetachedMiniButtonsData[_realm][_player][_FORCESCALE_] = 1
	end
end

function DMBS_ToggleLockAll()
	if (DetachedMiniButtonsData[_realm][_player][_LOCK_] == true) then
		DetachedMiniButtonsData[_realm][_player][_LOCK_] = false
	else
		DetachedMiniButtonsData[_realm][_player][_LOCK_] = true
	end
end

function DMBS_ToggleForceHidden()
	if (_sliderclick == true) then
		return
	end
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end
	local frame = getglobal(_lastSelectedFrame)
	if (frame == nil) then
		return
	end
	if (DetachedMiniButtonsData[_realm][_player][n][_FORCEHIDDEN_] == 1) then
		DetachedMiniButtonsData[_realm][_player][n][_FORCEHIDDEN_] = 0
		frame:Show()
	else
		DetachedMiniButtonsData[_realm][_player][n][_FORCEHIDDEN_] = 1
		frame:Hide()
	end
end

function DMBS_ToggleButtonLock()
	if (_sliderclick == true) then
		return
	end
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end
	local frame = getglobal(_lastSelectedFrame)
	if (frame == nil) then
		return
	end
	if (DetachedMiniButtonsData[_realm][_player][n][_LOCK_] == 1) then
		DetachedMiniButtonsData[_realm][_player][n][_LOCK_] = 0
	else
		DetachedMiniButtonsData[_realm][_player][n][_LOCK_] = 1
	end
end

function DMBSScrollBar_Update()
	local line -- 1 through 5 of our window to scroll
	local lineplusoffset -- an index into our data calculated from the scroll offset

	FauxScrollFrame_Update(DMBSScrollBar,table.getn(_sortedList),20,16)
	if (_sorted == false) then
		table.sort(_sortedList)
		_sorted = true
	end
	for line=1,20 do
		lineplusoffset = line + FauxScrollFrame_GetOffset(DMBSScrollBar)
		--DEFAULT_CHAT_FRAME:AddMessage(table.getn(_sortedList))
		if lineplusoffset <= table.getn(_sortedList)  then
			local s = _sortedList[lineplusoffset]
			if not s then
				s = ""
			end
			getglobal("DMBSEntry"..line.."_Text"):SetText(s)
			getglobal("DMBSEntry"..line):Show()
		else
			getglobal("DMBSEntry"..line):Hide()
		end
	end
end

function DMBSConfig_OnDragStart(self)
	self:StartMoving()
end

function DMBSConfig_OnDragStop(self)
	self:StopMovingOrSizing()
end

function DMBS_ButtonSliderChangedX()
	if (_sliderclick == true) then
		return
	end
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end
	local frame = getglobal(_lastSelectedFrame)
	if (frame == nil) then
		return
	end
	frame:ClearAllPoints()
	frame.currentX = DMBS_INTX(DMBS_SliderXPosition:GetValue())
	frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX, frame.currentY)
	DMBSTAB2_ButtonPos:SetText("X,Y: " .. frame.currentX .. "," .. frame.currentY)
	DetachedMiniButtonsData[_realm][_player][n][_LASTX_] = frame.currentX
end

function DMBS_ButtonSliderChangedY()
	if (_sliderclick == true) then
		return
	end
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end
	local frame = getglobal(_lastSelectedFrame)
	if (frame == nil) then
		return
	end
	frame:ClearAllPoints()
	frame.currentY = DMBS_INTY(DMBS_SliderYPosition:GetValue())
	frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX, frame.currentY)
	DMBSTAB2_ButtonPos:SetText("X,Y: " .. frame.currentX .. "," .. frame.currentY)
	DetachedMiniButtonsData[_realm][_player][n][_LASTY_] = frame.currentY

end

function DMBS_ButtonSliderScaleChanged()
	if (_sliderclick == true) then
		return
	end
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end

	local s = _lastSelectedFrame

	--if (DetachedMiniButtonsData[_realm][_player][n][_PARENT_] ~= nil) then
	--	s = DetachedMiniButtonsData[_realm][_player][n][_PARENT_]
	--end

	local frame = getglobal(s)
	if (frame == nil) then
		return
	end
	local scale = DMBS_SliderScale:GetValue()
	DMBSTAB2_ButtonScale:SetText("Scale: " .. scale .. "%")
	scale =  scale / 100
	DetachedMiniButtonsData[_realm][_player][n][_SCALE_] = scale
	DMBS_SetScale(scale,frame,n)
end

function DMBS_SetButtonData(s)
	_sliderclick = true
	DMBSTAB2_ButtonName:SetText("Button: " .. s)

	local frame = getglobal(s)
	if (frame == nil) then
		_sliderclick = false
		return
	end
	_lastSelectedFrame = s
	local x = DMBS_INTX(DMBS_RANGEX(frame:GetLeft()))
	local y = DMBS_INTY(DMBS_RANGEY(frame:GetTop()))
	DMBSTAB2_ButtonPos:SetText("X,Y: " .. x .. "," .. y)

	DMBS_SliderYPosition:SetValue(y)
	DMBS_SliderXPosition:SetValue(x)
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDY_] == 1) then
		DMBS_SliderYPosition:SetValueStep(DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_])
	else
		DMBS_SliderYPosition:SetValueStep(1)
	end
	if (DetachedMiniButtonsData[_realm][_player][_USEGRIDX_] == 1) then
		DMBS_SliderXPosition:SetValueStep(DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_])
	else
		DMBS_SliderXPosition:SetValueStep(1)
	end

	local scale = DetachedMiniButtonsData[_realm][_player][s][_SCALE_]
	if (scale == nil) then
		scale = 1
		DetachedMiniButtonsData[_realm][_player][s][_SCALE_] = scale
	end
	local sc = scale * 100
	DMBSTAB2_ButtonScale:SetText("Scale: " .. sc .. "%")
	DMBS_SliderScale:SetValue(sc)
	DMBS_ForceButtonHide:SetChecked(DetachedMiniButtonsData[_realm][_player][s][_FORCEHIDDEN_])
	DMBS_ForceButtonLock:SetChecked(DetachedMiniButtonsData[_realm][_player][s][_LOCK_])
	_sliderclick = false

end


function DMBSButtonList_Click(self)
	_sliderclick = true
	_lastSelectedFrame = nil
	local s = self:GetName()
	local _TAG_ = "DMBSEntry"
	local num = string.sub(s,strlen(_TAG_) + 1)
	local fin = num + FauxScrollFrame_GetOffset(DMBSScrollBar)
	s = _sortedList[fin]
	if (s == nil) then
		_sliderclick = false
		return
	end
	DMBS_SetButtonData(s)
	_sliderclick = false

end

function DMBS_TAB1Set()

	DMBSTAB1_USEGRIDFRAMEX:SetChecked(DetachedMiniButtonsData[_realm][_player][_USEGRIDX_])
	DMBS_GRIDSLIDERX:SetValue(DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_])
	DMBSTAB1_GRIDSIZETEXTX:SetText("Grid Size X: " .. DetachedMiniButtonsData[_realm][_player][_GRIDSIZEX_])
	DMBSTAB1_USEGRIDFRAMEY:SetChecked(DetachedMiniButtonsData[_realm][_player][_USEGRIDY_])
	DMBS_GRIDSLIDERY:SetValue(DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_])
	DMBSTAB1_GRIDSIZETEXTY:SetText("Grid Size Y: " .. DetachedMiniButtonsData[_realm][_player][_GRIDSIZEY_])
	DMBSTAB1_FORCESCALE:SetChecked(DetachedMiniButtonsData[_realm][_player][_FORCESCALE_])
	DMBSTAB1_LOCKALL:SetChecked(DetachedMiniButtonsData[_realm][_player][_LOCK_])
end

function DMBS_TAB2Set()
	DMBS_SliderYPositionHigh:SetText("" .. _UIMAXY_)
	DMBS_SliderXPositionHigh:SetText("" .. _UIMAXX_)
	DMBS_SliderXPositionLow:SetText("0")
	DMBS_SliderXPosition:SetMinMaxValues(0,_UIMAXX_)
	DMBS_SliderYPosition:SetMinMaxValues(0,_UIMAXY_)
	DMBS_SliderScaleHigh:SetText("200%")
	DMBS_SliderScaleLow:SetText("30%")
	DMBS_SliderScale:SetValueStep(1)
	DMBS_SliderScale:SetMinMaxValues(30,200)
end

function DMBS_TAB1Click()
	DMBS_TAB1Set()
	DMBS_DIALOGTAB2:Hide()
	DMBS_DIALOGTAB1:Show()
end

function DMBS_TAB2Click()
	DMBS_TAB2Set()
	DMBSScrollBar_Update()
	DMBS_DIALOGTAB1:Hide()
	DMBS_DIALOGTAB2:Show()
end

function DMBS_ButtonResetAll()
	DMBS_reset(_frameList)
end

function DMBS_ButtonCenter()
	if (_lastSelectedFrame == nil) then
		return
	end

	local n = DMBS_Mask(_lastSelectedFrame)

	if (not _frameList[n]) then
		return
	end

	local x = DMBS_INTX(_UIMAXX_ / 2)
	local y = DMBS_INTY(_UIMAXY_ / 2)

	local frame =DMBS_GETVALUE(n)
	if (frame) then
		frame:ClearAllPoints()
		frame.currentX = DMBS_RANGEX(x)
		frame.currentY = DMBS_RANGEY(y)
		frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX,frame.currentY)
		DetachedMiniButtonsData[_realm][_player][n][_LASTX_] = frame.currentX
		DetachedMiniButtonsData[_realm][_player][n][_LASTY_] = frame.currentY
	end
	DMBS_SetButtonData(_lastSelectedFrame)
end

function DMBS_ButtonReset()
	if (_lastSelectedFrame == nil) then
		return
	end
	local n = DMBS_Mask(_lastSelectedFrame)
	if (not _frameList[n]) then
		return
	end

	if (DetachedMiniButtonsData[_realm][_player][n]) then
		if (DetachedMiniButtonsData[_realm][_player][n][_INITIALX_] and 
			DetachedMiniButtonsData[_realm][_player][n][_INITIALY_]) then
			local frame =DMBS_GETVALUE(n)
			if (frame) then
				frame:ClearAllPoints()
				frame.currentX = DMBS_RANGEX(DMBS_INTX(DetachedMiniButtonsData[_realm][_player][n][_INITIALX_]))
				frame.currentY = DMBS_RANGEY(DMBS_INTY(DetachedMiniButtonsData[_realm][_player][n][_INITIALY_]))
				frame:SetPoint("TOPLEFT", UIParent, "BOTTOMLEFT",frame.currentX,frame.currentY)
				DetachedMiniButtonsData[_realm][_player][n][_LASTX_] = frame.currentX
				DetachedMiniButtonsData[_realm][_player][n][_LASTY_] = frame.currentY
			end
		end
	end
	DMBS_SetButtonData(_lastSelectedFrame)

end
----------------------------------------------------------------------------------------------------------
local _allrealms = nil
local _lastselectedchar = nil

function DMBS_CopyCharProfile()
	if (_lastselectedchar == nil) then
		return
	end
	local _,x = string.find(_lastselectedchar,"/")
	if (x == nil) then
		return
	end

	local r = string.sub(_lastselectedchar,0,x - 1)
	local c = string.sub(_lastselectedchar,x + 1)

	--DEFAULT_CHAT_FRAME:AddMessage(r)
	--DEFAULT_CHAT_FRAME:AddMessage(c)

	local list = {}

	for v in pairs(DetachedMiniButtonsData[r][c]) do 
	  if (type(DetachedMiniButtonsData[r][c][v]) == "table") then
			if (DetachedMiniButtonsData[_realm][_player][v] == nil) then
				DetachedMiniButtonsData[_realm][_player][v] = {}
			end
			list[v] = v
			for vv in pairs(DetachedMiniButtonsData[r][c][v]) do 
				DetachedMiniButtonsData[_realm][_player][v][vv] = DetachedMiniButtonsData[r][c][v][vv]
			end
	  else
			 DetachedMiniButtonsData[_realm][_player][v] = DetachedMiniButtonsData[r][c][v]
	  end
	end

	DMBS_rePosition(list)
	DMBS_cleanUp()

end

function DMBS_CharProfileSelected(self)
	--DEFAULT_CHAT_FRAME:AddMessage("DMBS_CharProfileSelected")

	if (self.value ~= nil) then
		_lastselectedchar = self.value
		--DEFAULT_CHAT_FRAME:AddMessage(self.value)
	end
	UIDropDownMenu_SetSelectedID(DMBSTAB1_COPYLIST, self:GetID())


end

function DMBS_SetCharDropDown()
	if (_allrealms == nil) then
		DMBS_getRealmChars()
	end
	if (_allrealms == nil) then
		return
	end
	local info = {}
	for _,c in pairs(_allrealms) do 
		info.text = c
		info.value = c
		info.func = DMBS_CharProfileSelected
		info.checked = nil
		UIDropDownMenu_AddButton(info)
	end
end

function DMBS_InitList()
	local dropdown = getglobal("DMBSTAB1_COPYLIST")
	UIDropDownMenu_Initialize(dropdown, DMBS_SetCharDropDown)
	UIDropDownMenu_SetSelectedID(dropdown,0)
end

function DMBS_getRealmChars()
	if (_allrealms ~= nil) then
		return
	end
	_allrealms = {}
	--DEFAULT_CHAT_FRAME:AddMessage("DMBS_getRealmChars")
	for r in pairs(DetachedMiniButtonsData) do
		for c in pairs(DetachedMiniButtonsData[r]) do 
			local s = r .. "/" .. c
			if (s ~= _realmchar) then
				--DEFAULT_CHAT_FRAME:AddMessage(s)
				table.insert(_allrealms,s)
			end
		end
	end
	table.sort(_allrealms)
end

function DMBS_CopyCommand(msg)
	if (not _initialized) then
		return
	end

	local _,x = string.find(msg," ")
	if (x == nil) then
		return
	end
	local s = string.sub(msg,x + 1)
	_,x = string.find(s,"/")
	if (x == nil) then
		s = _realm .. "/" .. s
	end
	if (_allrealms == nil) then
		DMBS_getRealmChars()
	end
	_lastselectedchar = s
	DMBS_CopyCharProfile()

end


